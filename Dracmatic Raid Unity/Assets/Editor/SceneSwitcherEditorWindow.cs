﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcherEditorWindow : EditorWindow
{
    #region Constants
    private const string HelpTooltip = "-Left Mouse on Scene Button to open the scene. \n" +
                                        "-Right Mouse on Scene Button to open the scene keeping last loaded scene on additive mode. \n" +
                                        "-Middle Mouse on Scene Button to load the scene in additive mode.\n" +
                                        "-Filter discards scene buttons that not contains spelled characters \n" +
                                        "-Selected Only Toggle to only display selected Scenes \n" +
                                        "-Lock Button to Lock/Unlock scene";

    private const string FilterText = "Filter: ";
    private const string SelectedOnlyText = "Selected Only:";
    private const string HelpIcon = "_Help";
    private const string UnlockedIcon = "LockIcon";
    private const string LockedIcon = "LockIcon-On";
    #endregion

    private string filterText;
    private bool selectedOnlyToogle;
    private Vector2 scrollPos;

    private GUIContent helpContent;
    private GUIContent HelpContent => helpContent ?? (helpContent = new GUIContent(EditorGUIUtility.IconContent(HelpIcon).image, HelpTooltip));

    private Dictionary<EditorBuildSettingsScene, bool> lockedScenes;
    private EditorBuildSettingsScene[] allScenes;

    [MenuItem("Basetis/SceneSwitcher")]
    public static void Open()
    {
        SceneSwitcherEditorWindow window = GetWindow<SceneSwitcherEditorWindow>();
        window.titleContent = new GUIContent("Scene Switcher");
        window.minSize = Vector2.one * 200;
        window.maxSize = Vector2.one * 400;
    }

    private void OnEnable()
    {
        OnSceneChanged();
        EditorBuildSettings.sceneListChanged += OnSceneChanged;
    }

    private void OnSceneChanged()
    {
        if (lockedScenes == null)
        {
            lockedScenes = new Dictionary<EditorBuildSettingsScene, bool>();
        }
        allScenes = EditorBuildSettings.scenes;
        foreach (var scene in allScenes)
        {
            if (!lockedScenes.ContainsKey(scene))
            {
                lockedScenes.Add(scene, false);
            }
        }
    }

    private void OnDisable()
    {
        EditorBuildSettings.sceneListChanged -= OnSceneChanged;
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(HelpContent, GUILayout.Width(20));
        EditorGUILayout.LabelField(FilterText, GUILayout.Width(35));
        filterText = EditorGUILayout.TextField(filterText);
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(SelectedOnlyText, GUILayout.Width(80));
        selectedOnlyToogle = EditorGUILayout.Toggle(selectedOnlyToogle);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();


        EditorGUILayout.BeginVertical();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
        var activeScene = SceneManager.GetActiveScene();

        for (int i = 0; i < allScenes.Length; i++)
        {
            var currentScene = allScenes[i];
            string path = currentScene.path;
            string sceneName = Path.GetFileNameWithoutExtension(currentScene.path);

            if (string.IsNullOrEmpty(sceneName))
                continue;
            if (!string.IsNullOrEmpty(filterText) && !sceneName.ToLower().Contains(filterText.ToLower()))
                continue;

            Scene scene = SceneManager.GetSceneByPath(path);
            if (selectedOnlyToogle && activeScene != scene && !IsSceneOpen(path, out Scene a))
                continue;

            if (activeScene == scene)
                GUI.backgroundColor = Color.green;
            else if (scene.isLoaded)
                GUI.backgroundColor = Color.yellow;
            else
                GUI.backgroundColor = Color.clear;

            EditorGUILayout.BeginHorizontal();
            bool lockedScene = lockedScenes[currentScene];
            if (GUILayout.Button(EditorGUIUtility.IconContent(lockedScene ? LockedIcon : UnlockedIcon).image, GUILayout.Width(30), GUILayout.Height(19)))
            {
                lockedScenes[currentScene] = lockedScene = !lockedScene;
            }
            bool GUIisEnabled = GUI.enabled;
            GUI.enabled = !lockedScene;
            if (GUILayout.Button(sceneName))
            {
                int currentButton = Event.current.button;

                if (currentButton == 0)
                    OnLeftMouseButtonPressed(path);
                else if (currentButton == 1)
                    OnRightMouseButtonPressed(path);
                else if (currentButton == 2)
                    OnMiddleMouseButtonPressed(path);
            }
            GUI.enabled = GUIisEnabled;

            EditorGUILayout.EndHorizontal();
        }
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
    }

    private void OnLeftMouseButtonPressed(string path)
    {
        OpenSingleScene(path);
    }

    private void OnRightMouseButtonPressed(string path)
    {
        bool isSceneOpen = IsSceneOpen(path, out Scene currentScene);

        if (!isSceneOpen)
        {
            OpenAdditiveScene(path);
            currentScene = SceneManager.GetSceneByPath(path);
        }
        SetActiveScene(currentScene);
    }

    private void OnMiddleMouseButtonPressed(string path)
    {
        bool isSceneOpen = IsSceneOpen(path, out Scene currentScene);

        if (isSceneOpen)
            CloseScene(currentScene);
        else
            OpenAdditiveScene(path);
    }

    private bool IsSceneOpen(string path, out Scene loadedScene)
    {
        loadedScene = SceneManager.GetSceneByPath(path);
        return loadedScene.isLoaded;
    }

    private void OpenSingleScene(string path)
    {
        OpenScene(path, OpenSceneMode.Single);
    }

    private void OpenAdditiveScene(string path)
    {
        OpenScene(path, OpenSceneMode.Additive);
    }

    private void OpenScene(string path, OpenSceneMode mode)
    {
        bool isSceneSaved = true;
        if (mode == OpenSceneMode.Single)
            isSceneSaved = EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();

        if (isSceneSaved)
            EditorSceneManager.OpenScene(path, mode);
    }

    private void SetActiveScene(Scene scene)
    {
        SceneManager.SetActiveScene(scene);
    }

    private void CloseScene(Scene scene)
    {
        if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            return;
        EditorSceneManager.CloseScene(scene, true);
    }
}