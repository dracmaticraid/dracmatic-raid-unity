using UnityEngine;

public class mapWaterMove : MonoBehaviour
{
    public bool isA;
    public float speed = 1;
    private float randomMult;

    private Renderer myRenderer;

    void Start()
    {
        myRenderer = GetComponent<Renderer>();
        randomMult = Random.Range(0.05f, 1.85f);
    }
    private void Update()
    {
        myRenderer.material.mainTextureOffset = new Vector2(isA ? -1 : 1, 0) * speed * randomMult * Time.time;
    }
}
