using UnityEngine;

public class CheckerboardUpdater : MonoBehaviour
{
    public Material[] checkerboardMaterials;

    void Update()
    {
        foreach (Material mat in checkerboardMaterials)
        {
            mat.SetVector("_PlayerPosition", transform.position);
        }
    }
}
