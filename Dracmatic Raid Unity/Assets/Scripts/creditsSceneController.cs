using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class creditsSceneController : MonoBehaviour
{

    void Start()
    {
        Invoke(nameof(toStart), 2f);
    }

    public void toStart(){
        GameManager.instance.sceneTransitioner.TransitionToScene("MainMenu");
    }
}
