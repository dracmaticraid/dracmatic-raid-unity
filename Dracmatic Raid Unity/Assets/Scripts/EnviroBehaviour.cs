using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;
using System.Linq;

public class EnviroBehaviour : MonoBehaviour
{
    public Transform cameraHolder;
    public Transform cameraTR;
    public LevelSelectorController levelSelector;
    Vector2 startPos;
    Vector2 actualPos;
    bool mouseDown = false;
    bool onMovement = false;
    float deltaPos;
    public GameObject caronte;
    Animator caronteAnim;
    public Vector3[] caronteStops; //0 = Level1; ---; 4 = Level5
    public Vector3[] cameraStops;
    Dictionary<int, Vector3> carontePaths = new Dictionary<int, Vector3>();
    Dictionary<int, Vector3> cameraPaths = new Dictionary<int, Vector3>();

    public float[] pathTimes;
    int selectedIndex;
    public int onStayIndex;
    int diferenceIndex;
    void Start(){
        onStayIndex = GameManager.instance.progress.unlockedLevels;
        cameraHolder.position = cameraStops[onStayIndex];
        caronte.transform.localPosition = caronteStops[onStayIndex];
        caronteAnim = caronte.GetComponentInChildren<Animator>();
        onMovement = false;
    }
    public void Update(){
        if(Input.GetMouseButtonDown(0)){ 
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit) && !onMovement){
                int nameIndex = Convert.ToInt32(hit.transform.name);
                if(GameManager.instance.progress.unlockedLevels >= nameIndex -1){
                    StartCoroutine(GoOnLevel(nameIndex)); //Raycast transform and get Name to use as an Index
                }
                else{ Debug.Log("Level "+ nameIndex + " Locked!"); }
            }
        }

        if(Input.GetMouseButtonDown(0)){
            startPos = Input.mousePosition;
            mouseDown = true;
        }
        if(mouseDown){
            actualPos = Input.mousePosition;
            deltaPos = actualPos.y - startPos.y;
            if(Mathf.Abs(deltaPos) > 0.5f){
                if(cameraTR.position.z >= -5f && cameraTR.position.z <= 1){
                    cameraTR.position += new Vector3(0, 0, -deltaPos * 0.0003f);
                }
                else if(cameraTR.position.z < -5f){
                    cameraTR.position = new Vector3(0, cameraTR.position.y, -5f);
                    mouseDown = false;
                    deltaPos = 0;
                }
                else if(cameraTR.position.z > 1){
                    cameraTR.position = new Vector3(0, cameraTR.position.y, 1);
                    mouseDown = false;
                    deltaPos = 0;
                }
            }
        }
        if(Input.GetMouseButtonUp(0)){
            mouseDown = false;
        }
    }

    IEnumerator GoOnLevel(int i){       
        onMovement = true;
        int intedLevelSelectIndex = i;
        selectedIndex = intedLevelSelectIndex -1;
        diferenceIndex = Mathf.Abs(onStayIndex - selectedIndex);
        if(diferenceIndex != 0){
#region CarontePath
        carontePaths.Clear();
        carontePaths.Add(0, caronteStops[onStayIndex]);
        if(selectedIndex > onStayIndex){
            for(int a = 1; a <= diferenceIndex; a ++){
                carontePaths.Add(a, caronteStops[a + onStayIndex]);
            }
        }        
        else if(selectedIndex < onStayIndex){
            for(int a = 1; a <= diferenceIndex; a ++){
                carontePaths.Add(a, caronteStops[Mathf.Abs(a - onStayIndex)]);
            }
        }
#endregion
#region CameraPath
        cameraPaths.Clear();
        cameraPaths.Add(0, cameraStops[onStayIndex]);
        if(selectedIndex > onStayIndex){
            for(int a = 1; a <= diferenceIndex; a ++){
                cameraPaths.Add(a, cameraStops[a + onStayIndex]);
            }
        }        
        else if(selectedIndex < onStayIndex){
            for(int a = 1; a <= diferenceIndex; a ++){
                cameraPaths.Add(a, cameraStops[Mathf.Abs(a - onStayIndex)]);
            }
        }
#endregion
        cameraTR.DOLocalMove(Vector3.zero, 1.5f).SetEase(Ease.InOutSine);  
        caronteAnim.SetTrigger("move");
        dynamicsPaths();
        yield return new WaitForSeconds(pathTimes[diferenceIndex]);
        caronteAnim.SetTrigger("idle");
        onStayIndex = selectedIndex;
        }
        onMovement = false;
        Debug.Log("Level"+i);
        levelSelector.OnLevel("Level"+i);
    }
    void dynamicsPaths(){
        caronte.transform.DOLocalPath(carontePaths.Values.ToArray(), pathTimes[diferenceIndex], PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.01f).SetEase(Ease.InOutSine);
        cameraHolder.DOLocalPath(cameraPaths.Values.ToArray(), pathTimes[diferenceIndex], PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.InOutSine);
    }
}
