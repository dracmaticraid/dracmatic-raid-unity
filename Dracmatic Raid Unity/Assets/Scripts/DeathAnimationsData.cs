using UnityEngine;

[CreateAssetMenu(fileName = "Death Anim Data", menuName = "Dracmatic Raid/Death Anim Data", order = 6)]
public class DeathAnimationsData : ScriptableObject
{
    public float timeInterImages;

    public Sprite[] caidaLago;
    public Sprite[] genericChoque;
    public Sprite[] corrupcion;
    public Sprite[] escudo;
    public Sprite[] mago;
    public Sprite[] tramposo;
    public Sprite[] necrofargo;
    public Sprite[] laser;
    public Sprite[] meteorito;
    public Sprite[] pobre;

}
