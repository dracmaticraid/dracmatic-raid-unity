using UnityEngine;

public class SwipeInputManager : MonoBehaviour
{
    PlayerController playerController;
    private Vector3 touchPosition;
    private Vector3 touchStart = Vector3.zero;
    private Vector3 touchDelta = Vector3.zero;
    void Awake()
    {
        playerController = gameObject.GetComponent<PlayerController>();
    }
    void Update()
    {
         if (Input.GetMouseButtonDown(0))
             playerController.RawInput();
         
         if (playerController.readyToStart && playerController.tikcsSincePlayerStart > 1)
         {
             if (Input.GetMouseButtonDown(0))
                 touchStart = Input.mousePosition;
         
             if (Input.GetMouseButton(0))
             {
                 touchPosition = Input.mousePosition;
                 touchDelta = touchPosition - touchStart;
                 if (touchDelta.magnitude > .197f * Screen.dpi)
                 {
                     var x = (touchDelta.x);
                     var y = (touchDelta.y);
                     if (Mathf.Abs(x) > Mathf.Abs(y))
                     {
                         if (x > 0)
                             playerController.SwipeRight();
                         else
                             playerController.SwipeLeft();
                     }
                     else
                     {
                         if (y > 0)
                             playerController.SwipeUp();
                         else
                             playerController.SwipeDown();
                     }
                     touchStart = touchPosition;
                 }
                 touchPosition.z = 10.0f; //distance of the plane from the camera
             }
         }
    }
}
