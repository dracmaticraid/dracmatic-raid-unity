using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIConfigController : MonoBehaviour
{
public TextMeshProUGUI speedDebug;

    private void Start() {
        speedDebug.text = Time.fixedDeltaTime.ToString();
    }

    //Modify Fixed Step Time through buttons on UI
    public void FixedTimeAdd() {
        Time.fixedDeltaTime += 0.01f;
        speedDebug.text = Time.fixedDeltaTime.ToString();
    }
    public void FixedTimeSubtract() {
        if(Time.fixedDeltaTime > 0.02f)
                Time.fixedDeltaTime -= 0.01f;
        speedDebug.text = Time.fixedDeltaTime.ToString();
    }

    public void OnToMap(){
        GameManager.instance.sceneTransitioner.TransitionToScene("LevelSelector");
    }
}
