using DG.Tweening;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{
    public PlayerController player;
    public SwipeInputManager swipeInputManager;
    public TutorialData tutorialData;

    public GameObject tutorialPanel;
    public Image caronteImage;
    public TextMeshProUGUI mainText;

    public Vector3[] coords;

    public bool interactionInput;

    private void Start()
    {
        GameManager.instance.player.canInput = false;
        GameManager.instance.player.camPan.inPan = true;


        StartCoroutine(Tutorial());
    }
    public void TutorialActionButton()
    {
        interactionInput = true;
    }
    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
            TutorialActionButton();
    }
    IEnumerator Tutorial()
    {
        player.GetComponent<SwipeInputManager>().enabled = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[0];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[1];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[2];
        GameManager.instance.player.camPan.inPan = false;
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[3];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        tutorialPanel.SetActive(false);
        player.GetComponent<SwipeInputManager>().enabled = true;
        GameManager.instance.player.RawInput();

        GameManager.instance.player.canInput = false;
        while (GameManager.instance.player.transform.position.x < coords[0].x)
            yield return null;
        Time.timeScale = 0;

        //Dialogue 1; to Down
        interactionInput = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[4];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[5];

        GameManager.instance.player.canInput = true;
        while (GameManager.instance.player.inversePlayerOrientation != Orientations.Down)
        {
            GameManager.instance.player.hasChangedDirection = false;
            yield return null;
        }
        GameManager.instance.player.canInput = false;
        Time.timeScale = 1;
        interactionInput = false;
        tutorialPanel.SetActive(false);

        while (GameManager.instance.player.transform.position.z > coords[1].z)
            yield return null;
        Time.timeScale = 0;

        //Dialogue 2; to Right
        interactionInput = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[6];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[7];

        GameManager.instance.player.canInput = true;
        while (GameManager.instance.player.inversePlayerOrientation != Orientations.Left)
        {
            GameManager.instance.player.hasChangedDirection = false;
            yield return null;
        }
        interactionInput = false;
        tutorialPanel.SetActive(false);
        GameManager.instance.player.canInput = false;
        Time.timeScale = 1;

        while (GameManager.instance.player.transform.position.x < coords[2].x)
            yield return null;
        Time.timeScale = 0;

        //Dialogue 3; to Interact
        interactionInput = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[8];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[9];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[10];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[11];

        GameManager.instance.player.canInput = true;
        while (!GameManager.instance.player.interactionInput)
            yield return null;
        interactionInput = false;
        tutorialPanel.SetActive(false);
        GameManager.instance.player.canInput = false;
        Time.timeScale = 1;

        while (GameManager.instance.player.transform.position.x < coords[3].x)
            yield return null;
        Time.timeScale = 0;

        //Dialogue 4; to Up
        interactionInput = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[12];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[13];

        GameManager.instance.player.canInput = true;
        while (GameManager.instance.player.inversePlayerOrientation != Orientations.Top)
        {
            GameManager.instance.player.hasChangedDirection = false;
            yield return null;
        }
        interactionInput = false;
        tutorialPanel.SetActive(false);
        GameManager.instance.player.canInput = false;
        Time.timeScale = 1;

        while (GameManager.instance.player.transform.position.z < coords[4].z)
            yield return null;
        Time.timeScale = 0;

        //Dialogue 5; to nothing
        interactionInput = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[14];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[15];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[16];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[17];
        while (!interactionInput)
            yield return null;
        GameManager.instance.player.canInput = false;
        Time.timeScale = 1;
        interactionInput = false;
        tutorialPanel.SetActive(false);
        while (GameManager.instance.player.transform.position.z < coords[5].z)
            yield return null;
        Time.timeScale = 0;

        //Dialogue 6; to nothing
        interactionInput = false;
        tutorialPanel.SetActive(true);
        mainText.text = tutorialData.dialogues[18];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[19];
        while (!interactionInput)
            yield return null;
        interactionInput = false;
        mainText.text = tutorialData.dialogues[20];
        while (!interactionInput)
            yield return null;
        interactionInput = false;

        GameManager.instance.player.canInput = true;
        Time.timeScale = 1;
        tutorialPanel.SetActive(false);
    }
}
