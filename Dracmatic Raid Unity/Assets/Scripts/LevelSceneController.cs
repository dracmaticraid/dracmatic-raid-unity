using UnityEngine;

public class LevelSceneController : MonoBehaviour
{
    public int levelIndex;

    public void OnGoToMap()
    {
        Time.timeScale = 1;
        GameManager.instance.player.readyToStart = false;
        GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen("ScrollLevelSelector");
        GameManager.SaveConfig(GameManager.instance.config);
    }
    public void OnWin()
    {
        Invoke(nameof(OnGoToMap), 1f);
    }
}
