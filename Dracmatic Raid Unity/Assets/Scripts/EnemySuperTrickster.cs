using UnityEngine;

public class EnemySuperTrickster : EnemySpecialBehaviour
{
    int trickIndex = 0;
    public GameObject fakeDeathPS;

    public override void Interact()
    {
        if (trickIndex < baseBehaviour.superTricksterNewPoses.Length)
            TrickMoment();
        else
        {
            GameManager.instance.player.GetChicken();
            baseBehaviour.Die();
        }
    }
    public void TrickMoment()
    {
        GameObject fakeDeathPSGO = Instantiate(fakeDeathPS, transform.position, Quaternion.Euler(new Vector3(-90f, 0, 0)));
        Destroy(fakeDeathPSGO, 1f);

        GameManager.instance.currentLevel.SwipeObjectFromToWho(gameObject, baseBehaviour.superTricksterNewPoses[trickIndex]); //Matrix movement
        transform.position = new Vector3(baseBehaviour.superTricksterNewPoses[trickIndex].x, transform.position.y, baseBehaviour.superTricksterNewPoses[trickIndex].y); //InGame movement
        trickIndex++;
    }
}