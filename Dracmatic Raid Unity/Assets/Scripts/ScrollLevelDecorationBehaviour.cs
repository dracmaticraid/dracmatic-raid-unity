using UnityEngine;

public class ScrollLevelDecorationBehaviour : MonoBehaviour
{
    public ScrollLevelDecorationAsset[] decorationAssets;
    public RectTransform levelCountainer;

    public float topPos;
    public float botPos;

    void Awake()
    {
        decorationAssets = FindObjectsOfType<ScrollLevelDecorationAsset>();
    }

    void Start()
    {
        foreach (ScrollLevelDecorationAsset decorationAsset in decorationAssets)
        {
            botPos = topPos - decorationAsset.decoMinPos;
        }
    }
    public float LerpFuntionCalculator(float A, float B)
    {
        float dedicatedLerp = Mathf.Lerp(A, B, (levelCountainer.anchoredPosition.y - botPos) / (topPos - botPos));
        return dedicatedLerp;
    }
}
