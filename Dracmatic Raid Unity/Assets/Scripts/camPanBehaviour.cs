using DG.Tweening;
using UnityEngine;

public class camPanBehaviour : MonoBehaviour
{
    LevelData levelData;
    private Vector3[] points;
    
    private float time;
    public bool inPan = false;
    void Start()
    {
        levelData = GameManager.instance.currentLevel;
        points = levelData.panPoints;
        time = levelData.panTime;
    }

    void Update()
    {
        if (!inPan && !GameManager.instance.player.readyToStart && !GameManager.instance.player.levelEnded)
        {
            inPan = true;
            InvokeRepeating("Init", 1, time);
        }
    }
    public void Init()
    {
        transform.DOLocalPath(points, time, PathType.CatmullRom, PathMode.Full3D).SetEase(Ease.Linear);
    }

    public void resetTransform()
    {
        CancelInvoke();
        DOTween.Kill(transform);
        transform.localPosition = Vector3.zero;
    }
}
