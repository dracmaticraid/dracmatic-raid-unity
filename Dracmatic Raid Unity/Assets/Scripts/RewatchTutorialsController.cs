using UnityEngine;
using UnityEngine.UI;

public class RewatchTutorialsController : MonoBehaviour
{
    public Transform tutorials;
    public void Volver()
    {
        GameManager.instance.sceneTransitioner.TransitionToScene("ScrollLevelSelector");
    }

    public void Start()
    {
        if (GameManager.instance.progress.unlockedLevels >= 2)
            tutorials.GetChild(1).GetComponent<Button>().interactable = true;
        else
            tutorials.GetChild(1).GetComponent<Button>().interactable = false;

        if (GameManager.instance.progress.unlockedLevels >= 4)
            tutorials.GetChild(2).GetComponent<Button>().interactable = true;
        else
            tutorials.GetChild(2).GetComponent<Button>().interactable = false;

        if (GameManager.instance.progress.unlockedLevels >= 6)
            tutorials.GetChild(3).GetComponent<Button>().interactable = true;
        else
            tutorials.GetChild(3).GetComponent<Button>().interactable = false;

    }

    public void GoTutorialLevel(string levelName)
    {
        GameManager.instance.commingFromRewatchTutorials = true;
        GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen(levelName);
    }
}
