using UnityEngine;

public class magicModeTile : MonoBehaviour
{
    private int tinkint = 1;

    void Update()
    {
        var color = Color.white;

        tinkint = Random.Range(1, 6);

        if (GetComponentInChildren<MeshRenderer>() != null)
        {
            switch (tinkint)
            {
                case 1:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("_extraColor", Color.red);
                    return;
                case 2:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("_extraColor", Color.yellow);
                    return;
                case 3:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("_extraColor", Color.green);
                    return;
                case 4:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("_extraColor", Color.blue);
                    return;
                case 5:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("_extraColor", Color.cyan);
                    return;
                case 6:
                    GetComponentInChildren<MeshRenderer>().material.SetColor("_extraColor", Color.magenta);
                    return;
            }
        }
    }
}
