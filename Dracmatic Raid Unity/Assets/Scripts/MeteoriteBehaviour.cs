using DG.Tweening;
using System.Collections;
using UnityEngine;

public class MeteoriteBehaviour : MonoBehaviour
{
    public int timeToFall;
    public Vector2 spawnPos;
    bool isFalling = false;
    public Transform metGide;
    public Transform metGeo;

    void Update()
    {
        if (GameManager.instance.player.readyToStart)
        {
            if (!isFalling && Vector3.Distance(new Vector3(transform.position.x, 0, transform.position.z), GameManager.instance.player.transform.position)
                <= GameManager.instance.levelController.levelSpawner.configAssets.meteoriteDistanceTrigger)
            {
                StartCoroutine(MeteoriteFall());
                isFalling = true;
            }
        }
    }

    //Child index 0 = MeteoGeo, index 1 = meteoGide
    IEnumerator MeteoriteFall()
    {
        GameObject tile = GameManager.instance.currentLevel.GetTileAt(spawnPos);
        MeshRenderer tileBaseRender = tile.GetComponentInChildren<MeshRenderer>();
        float metFallTime = GameManager.instance.levelController.levelSpawner.configAssets.meteoriteFallTime;
        float metDecompTime = GameManager.instance.levelController.levelSpawner.configAssets.meteoriteDecomSpeed;
        metGeo.DOMoveY(0, metFallTime, false).SetEase(Ease.Linear).OnComplete(() => Destroy(metGeo.gameObject));
        metGide.DOScale(new Vector3(0.1f, 0.1f, 0.1f), metFallTime);
        yield return new WaitForSeconds(metFallTime);

        GameObject residualMeteorite = Instantiate(GameManager.instance.levelController.levelSpawner.meteoritePS, tile.transform.position + new Vector3(0, -0.5f, 0), Quaternion.identity);
        residualMeteorite.transform.DOScale(Vector3.zero, metDecompTime).SetEase(Ease.InExpo);
        tile.tag = "meteoriteTile";
        if (GameManager.instance.currentLevel.GetObjectAt(spawnPos) != null)
            GameManager.instance.currentLevel.GetObjectAt(spawnPos).GetComponent<EnemySpecialBehaviour>().HitByMeteorite();
        yield return new WaitForSeconds(metDecompTime);

        Destroy(residualMeteorite);
        tile.tag = "BasicTile";
        Destroy(gameObject);
    }
}
