using UnityEngine;
using UnityEngine.UI;

public class RewatchCinematicsController : MonoBehaviour
{
    public Transform cinematics;
    public void Volver()
    {
        GameManager.instance.sceneTransitioner.TransitionToScene("ScrollLevelSelector");
    }

    public void Start()
    {
        for (int i = 0; i < cinematics.childCount; i++)
        {
            if (i < GameManager.instance.progress.watchedCinematics.Count)
                cinematics.GetChild(i).GetComponent<Button>().interactable = true;
            else
                cinematics.GetChild(i).GetComponent<Button>().interactable = false;
        }
    }
    public void RewatchCinematic(int cinematicIndex)
    {
        GameManager.instance.cinematicFromRewatch = true;
        GameManager.instance.currentLevelIndex = cinematicIndex;
        GameManager.instance.sceneTransitioner.TransitionToScene("CinematicScene");
    }
}
