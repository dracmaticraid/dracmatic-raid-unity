using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class SceneTransitioner : MonoBehaviour
{
    public RawImage transitionImage;
    public CanvasGroup loadingScreen;
    
    public float transitionTime = .5f;

    void Start () {
        DG.Tweening.DOTween.KillAll();
        // Init transition image
        transitionImage.color = new Vector4(0,0,0,0);
        transitionImage.raycastTarget = false;

        // Init loading screen
        loadingScreen.alpha = 0;
        loadingScreen.blocksRaycasts = false;
    }

    public void TransitionToScene(string scene) {
        transitionImage.raycastTarget = true;
        transitionImage.DOFade(1, transitionTime).SetUpdate(true).SetEase(Ease.InQuad).OnComplete(()=>{
            SceneManager.LoadScene(scene);
            transitionImage.raycastTarget = false;
            transitionImage.DOFade(0,transitionTime).SetEase(Ease.OutQuad);
        });
    }

    public void TransitionWithLoadingScreen(string scene) {
        loadingScreen.blocksRaycasts = true;
        loadingScreen.DOFade(1, transitionTime).SetUpdate(true).SetEase(Ease.InQuad).OnComplete(()=>{
            StartCoroutine(LoadSceneAsync(scene));
        });
    }

    IEnumerator LoadSceneAsync(string scene) {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(scene);
        float startTime = Time.unscaledTime;

        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            //Debug.Log("Time.unscaledTime " + Time.unscaledTime);
            yield return null;
        }

        loadingScreen.blocksRaycasts = false;
        loadingScreen.DOFade(0,transitionTime).SetUpdate(true).SetEase(Ease.OutQuad);
    }
}
