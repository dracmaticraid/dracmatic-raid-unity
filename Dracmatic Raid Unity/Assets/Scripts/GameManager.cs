using Sirenix.OdinInspector;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public enum Cinematics { intro, caronte, preCerbero, postCerbero, creditos }
public enum ControlSchemes { swipe, button }
public enum Orientations { Top, Right, Down, Left }

[System.Serializable]
public class GameConfig
{
    public ControlSchemes controlScheme = ControlSchemes.swipe;

    public bool musicEnabled = true;
    public bool sfxEnabled = true;
    public float inputSensitivity = .5f;
}

[System.Serializable]
public class GameProgress
{
    public int unlockedLevels = 0;
    public int watchedTutorials = 0;
    public List<Cinematics> watchedCinematics;
}

[System.Serializable]
public struct Levels
{
    public LevelData levelData;
    public string levelName;
    public GameObject cinematic;
}

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public PlayerController player; //Given on Start from PlayerController
    public LevelController levelController; //Given on Start from LevelController
    public LevelData currentLevel; //Given on Start from LevelSpawner
    public int currentLevelIndex; //Given on enter a level from levelSelector ||  by rewatch cinematic
    public SceneTransitioner sceneTransitioner;
    public GameObject sceneTransitionerPrefab;

    public GameConfig config;
    public GameProgress progress;
    public bool caronteOnEndPos = false;

    public Levels[] levels;

    public bool cinematicFromRewatch = false;
    public bool cinematicFromIntro = false;
    public bool cinematicFromGameEnd = false;
    public bool cinematicFromFinish = false;

    public bool commingFromRewatchTutorials = false;


    //float GUIposition = 0;

    void Awake()
    {
        config = LoadConfig();
        progress = LoadProgress();


        // Singleton logic
        if (GameManager.instance != null)
        {
            DestroyImmediate(gameObject);
            return;
        }
        instance = this;
        DontDestroyOnLoad(this);

        caronteOnEndPos = false;
        if (sceneTransitioner == null)
        {
            sceneTransitioner = Instantiate(sceneTransitionerPrefab, transform).GetComponent<SceneTransitioner>();
        }
    }

    //void Update () {
    //    GUIposition = 0;
    //}

    #region helper funcions
    public static Vector3 GetXYWorldPosition(int x, int y)
    {
        return new Vector3(x, 0, y);
    }

    public static Vector2 Get2DPosition(Vector3 worldPosition)
    {
        return new Vector2(worldPosition.x, worldPosition.z);
    }

    public static Vector2 Get2DGridPosition(Vector3 worldPosition)
    {
        return new Vector2(Mathf.Round(worldPosition.x), Mathf.Round(worldPosition.z));
    }
    #endregion

    #region Game Settings
    public static GameConfig LoadConfig()
    {

        string filePath = Application.persistentDataPath + "/config.drc";
        Debug.Log(filePath);

        if (File.Exists(filePath))
        {
            string configJson = File.ReadAllText(Application.persistentDataPath + "/config.drc");
            Debug.Log("Loaded config data.");
            GameConfig _config = JsonUtility.FromJson<GameConfig>(configJson);
            return _config;
        }
        else
        {
            Debug.Log("Could not find saved config data.");
            return new GameConfig();
        }
    }
    public static void SaveConfig(GameConfig config)
    {
        string configJson = JsonUtility.ToJson(config);
        File.WriteAllText(Application.persistentDataPath + "/config.drc", configJson);
        Debug.Log("Config saved!: " + configJson);
    }
    #endregion

    #region Game Progress
    public static GameProgress LoadProgress()
    {
        string filePath = Application.persistentDataPath + "/progress.drc";
        if (File.Exists(filePath))
        {
            string progressJson = File.ReadAllText(Application.persistentDataPath + "/progress.drc");
            Debug.Log("Loaded progress data.");
            GameProgress _progress = JsonUtility.FromJson<GameProgress>(progressJson);
            return _progress;
        }
        else
        {
            Debug.Log("Could not find saved progress data.");
            return new GameProgress();
        }
    }
    public static void SaveProgress(GameProgress config)
    {
        string progressJson = JsonUtility.ToJson(config);
        File.WriteAllText(Application.persistentDataPath + "/progress.drc", progressJson);
        Debug.Log("Progress saved!: " + progressJson);
    }

    public static void DeleteProgress(GameProgress config)
    {
        string filePath = Application.persistentDataPath + "/progress.drc";
        if (File.Exists(filePath))
        {
            File.Delete(filePath);
            Debug.Log("Progress Deleted!");
        }
    }
    #endregion

    #region Debug
    //public void DebugText(string s) {
    //    GUIposition += 20;
    //    GUI.Label(new Rect(20, GUIposition, 200, 70), s);
    //}
    #endregion

    [Button]
    public void EditorSaveProgress()
    {
        SaveProgress(progress);
    }
}
