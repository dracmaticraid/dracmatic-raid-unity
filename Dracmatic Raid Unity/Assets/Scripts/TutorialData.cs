using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Tutorial Data", menuName = "Dracmatic Raid/Tutorial Data", order = 4)]
public class TutorialData : ScriptableObject
{
    public string[] dialogues;
    public Vector2[] spritesPos;
    public Vector2[] facePos;
    public float[] spriteZRot;
    public float[] spriteXScale;
}
