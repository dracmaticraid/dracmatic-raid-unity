using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RasgaAlmasBehaviour : MonoBehaviour
{
    public Animator animator;
    void Start(){
        float i = Random.Range(0.80f, 1.2f);
        animator.speed *= i;
    }

    public void Hide(){
        animator.SetTrigger("Hide");
    }
}
