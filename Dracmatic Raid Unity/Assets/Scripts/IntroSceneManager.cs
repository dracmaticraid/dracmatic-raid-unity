using UnityEngine;

public class IntroSceneManager : MonoBehaviour
{
    public GameObject blackImage;
    
    private void Start()
    {
        if (!GameManager.instance.progress.watchedCinematics.Contains(Cinematics.intro))
        {
            GameManager.instance.cinematicFromIntro = true;
            GameManager.instance.currentLevelIndex = 10;
            GameManager.instance.sceneTransitioner.TransitionToScene("CinematicScene");
        }
        else
            blackImage.SetActive(false);
    }
    public void OnGameImgButtonClick()
    {
        GameManager.instance.sceneTransitioner.TransitionToScene("ScrollLevelSelector");
    }

    public void DeleteContent()
    {
        GameManager.instance.progress.unlockedLevels = 0;
        GameManager.instance.progress.watchedCinematics.Clear();
        GameManager.instance.progress.watchedTutorials = 0;

        GameManager.SaveProgress(GameManager.instance.progress);
        GameManager.LoadProgress();
    }
    public void FullContent()
    {
        GameManager.instance.progress.unlockedLevels = 999;
        GameManager.instance.progress.watchedCinematics.Add(Cinematics.intro);
        GameManager.instance.progress.watchedCinematics.Add(Cinematics.caronte);
        GameManager.instance.progress.watchedCinematics.Add(Cinematics.preCerbero);
        GameManager.instance.progress.watchedCinematics.Add(Cinematics.postCerbero);
        GameManager.instance.progress.watchedCinematics.Add(Cinematics.creditos);
        GameManager.instance.progress.watchedTutorials = 999;

        GameManager.SaveProgress(GameManager.instance.progress);
        GameManager.LoadProgress();
    }
}


