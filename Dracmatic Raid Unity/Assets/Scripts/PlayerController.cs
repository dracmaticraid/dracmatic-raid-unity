using Cinemachine;
using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using static UIManager;

public class PlayerController : MonoBehaviour
{
    #region Consts
    public const string horizontal = "Horizontal";
    public const string vertical = "Vertical";

    public const string animRun = "run";
    public const string animAtt = "attack";
    public const string animEnterBoat = "enterBoat";
    public const string animDeathByNecro = "dNecro";
    public const string animDeathByLago = "dLago";
    public const string animDeathByChoque = "dChoque";
    public const string animPower = "power";

    public const string basicTile = "BasicTile";
    public const string obstacle = "Obstacle";
    public const string lasered = "Lasered";
    public const string end = "end";
    public const string decoration = "Decoration";
    public const string meteoriteTile = "meteoriteTile";
    public const string enemy = "Enemy";

    public const string interact = "Interact";

    public const string isPulsing = "_isPulsing";

    public const string cinematicScene = "CinematicScene";
    #endregion

    public Transform playerBody;
    public Animator playerAnim;
    public Renderer meshRenderer;
    public PlayerSettings settings;

    public LevelSceneController levelSceneController;
    private CinemachineImpulseSource source;
    public camPanBehaviour camPan;
    public UIManager uiManager;

    public GameObject readyPanel;
    public GameObject explosionPS;
    public GameObject furtherTile;
    public GameObject chicken;
    public Transform pivotForChicken;

    public Vector2 position = Vector2.zero;
    public Vector2 direction;

    public Orientations inversePlayerOrientation;

    public bool readyToStart = false;
    public bool levelEnded = false;
    public bool chickenOnHands = false;
    public bool willMove = true;
    public bool isPowered = false;
    public bool canInput = true;
    public bool interactionInput;
    public bool hasChangedDirection = false;
    private bool canChangeDirection = true;
    private bool losed = false;


    public int tikcsSincePlayerStart = 0;
    public int enemiesToDefeat;
    public int enemiesDefeated = 0;
    private int ticksStoppedNecrofargo = 0;
    private int ticksStoppedMago = 0;
    private int ticksStoppedEscudo = 0;
    private int ticksStoppedTramposo = 0;
    private int ticksStoppedMeteorito = 0;
    private int ticksStoppedWater = 0;
    private int ticksStoppedDecoration = 0;

    public float inputBufferTime;

    void Awake()
    {
        GameManager.instance.player = this;
    }
    void Start()
    {
        chickenOnHands = false;
        playerAnim.SetBool("withChicken", false);
        position = new Vector2(transform.position.x, transform.position.z);
        direction = GameManager.instance.currentLevel.startDirection;

        readyPanel.SetActive(false);
        source = GetComponent<CinemachineImpulseSource>();
        uiManager = GetComponentInChildren<UIManager>();
    }

    public void StartLevel()
    {
        SetPosition(GameManager.instance.currentLevel.startPosition);
        enemiesToDefeat = GameManager.instance.currentLevel.numberOfDracmasToCompleteLevel;
    }

    IEnumerator ReadyToStart()
    {
        camPan.resetTransform();
        readyPanel.SetActive(true);
        yield return new WaitForSecondsRealtime(3f);
        readyPanel.SetActive(false);
        readyToStart = true;
        playerAnim.SetTrigger(animRun);

    }

    void Update()
    {
        #region PC Input
        if (Input.GetKeyDown(KeyCode.Space))
            RawInput();

        if (canInput)
        {
            float hInput = Input.GetAxis(horizontal);
            float vInput = Input.GetAxis(vertical);

            if ((direction == Vector2.up || direction == Vector2.down) && canChangeDirection && hInput != 0 && !hasChangedDirection)
            {
                direction = new Vector2(Mathf.RoundToInt(hInput), 0);
                hasChangedDirection = true;

                if (hInput > 0)
                    inversePlayerOrientation = Orientations.Left;
                else
                    inversePlayerOrientation = Orientations.Right;

                ResetTicks();
            }

            if ((direction == Vector2.left || direction == Vector2.right) && canChangeDirection && vInput != 0 && !hasChangedDirection)
            {
                direction = new Vector2(0, Mathf.RoundToInt(vInput));
                hasChangedDirection = true;

                if (vInput > 0)
                    inversePlayerOrientation = Orientations.Top;
                else
                    inversePlayerOrientation = Orientations.Down;

                ResetTicks();
            }
        }
        #endregion
    }
    #region CellPhone Input
    public void SwipeUp()
    {
        if (!canInput)
            return;

        if (direction == Vector2.left || direction == Vector2.right && canChangeDirection && !hasChangedDirection)
        {
            direction = Vector2.up;
            inversePlayerOrientation = Orientations.Top;
            hasChangedDirection = true;
            ResetTicks();
        }
    }
    public void SwipeDown()
    {
        if (!canInput)
            return;

        if (direction == Vector2.left || direction == Vector2.right && canChangeDirection && !hasChangedDirection)
        {
            direction = Vector2.down;
            inversePlayerOrientation = Orientations.Down;
            hasChangedDirection = true;
            ResetTicks();
        }
    }
    public void SwipeLeft()
    {
        if (!canInput)
            return;

        if ((direction == Vector2.up || direction == Vector2.down) && canChangeDirection && !hasChangedDirection)
        {
            direction = Vector2.left;
            inversePlayerOrientation = Orientations.Right;
            hasChangedDirection = true;
            ResetTicks();
        }
    }
    public void SwipeRight()
    {
        if (!GameManager.instance.player.readyToStart || !canInput)
            return;

        if ((direction == Vector2.up || direction == Vector2.down) && canChangeDirection && !hasChangedDirection)
        {
            direction = Vector2.right;
            inversePlayerOrientation = Orientations.Left;
            hasChangedDirection = true;
            ResetTicks();
        }
    }
    #endregion

    public void RawInput()
    {
        if (!levelEnded && !losed)
        {
            if (!readyToStart)
                StartCoroutine(ReadyToStart());
            else
            {
                interactionInput = true;
                Invoke(nameof(InputBuffer), inputBufferTime);
            }
        }
    }
    void InputBuffer()
    {
        interactionInput = false;
    }
    void ResetTicks()
    {
        ticksStoppedNecrofargo = 0;
        ticksStoppedMeteorito = 0;
        ticksStoppedWater = 0;
    }

    void FixedUpdate()
    {
        if (readyToStart)
        {
            tikcsSincePlayerStart += 1;
            willMove = true;

            GameObject nextTile = GameManager.instance.currentLevel.GetTileAt(position + direction);
            furtherTile = GameManager.instance.currentLevel.GetTileAt(position + direction * 2);
            GameObject currentTile = GameManager.instance.currentLevel.GetTileAt(position);
            GameObject objectInNextTile = GameManager.instance.currentLevel.GetObjectAt(position + direction);

            if (currentTile.CompareTag(basicTile))
                GameManager.instance.levelController.SwitchTileToObstacle(position);

            if (nextTile == null)
            {
                willMove = false;
                ticksStoppedWater += 1;
            }
            else if (nextTile.CompareTag(obstacle))
            {
                if (isPowered)
                {
                    meshRenderer.material.SetFloat(isPulsing, 0);
                    playerAnim.SetTrigger(animPower);
                    willMove = true;
                    isPowered = false;
                    GameObject explosionPSGO = Instantiate(explosionPS, transform.position, Quaternion.Euler(new Vector3(-90f, 0, 0)));
                    Destroy(explosionPSGO, 1f);
                    source.GenerateImpulse();
                }
                else
                {
                    playerAnim.SetTrigger(animDeathByChoque);
                    Lose(DeathEnemySource.corrupcion);
                }
            }
            else if (nextTile.CompareTag(lasered))
            {
                if (isPowered)
                {
                    meshRenderer.material.SetFloat(isPulsing, 0);
                    playerAnim.SetTrigger(animPower);
                    willMove = true;
                    isPowered = false;
                    GameObject explosionPSGO = Instantiate(explosionPS, transform.position, Quaternion.Euler(new Vector3(-90f, 0, 0)));
                    Destroy(explosionPSGO, 1f);
                    source.GenerateImpulse();
                }
                else
                {
                    playerAnim.SetTrigger(animDeathByChoque);
                    Lose(DeathEnemySource.laser);
                }
            }
            else if (nextTile.CompareTag(end))
            {
                if (enemiesDefeated == enemiesToDefeat && GameManager.instance.caronteOnEndPos || levelSceneController.levelIndex == 8 && chickenOnHands)
                    Victory();
                else
                {
                    if (GameManager.instance.caronteOnEndPos)
                    {

                        playerAnim.SetTrigger(animDeathByNecro);
                        Lose(DeathEnemySource.pobre);
                    }
                    else
                    {
                        playerAnim.SetTrigger(animDeathByLago);
                        Lose(DeathEnemySource.lago);
                    }
                }
            }
            else if (nextTile.CompareTag(meteoriteTile))
            {
                willMove = false;
                ticksStoppedMeteorito += 1;
            }
            else if (nextTile.CompareTag(decoration))
            {
                willMove = false;
                ticksStoppedDecoration += 1;
            }

            if (objectInNextTile != null)
            {
                if (objectInNextTile.CompareTag(enemy))
                {
                    EnemyBehaviour.EnemyKind enemyKind = objectInNextTile.GetComponent<EnemyBehaviour>().enemyKind;

                    canChangeDirection = false;

                    if (interactionInput)
                    {
                        source.GenerateImpulse();

                        playerAnim.SetTrigger(animAtt);
                        objectInNextTile.SendMessage(interact);

                        canChangeDirection = true;
                        willMove = true;
                        interactionInput = false;

                        if (enemyKind == EnemyBehaviour.EnemyKind.Basic || enemyKind == EnemyBehaviour.EnemyKind.Trickster)
                            uiManager.AddDracmasToUI();

                        ResetTicks();
                    }
                    else
                    {
                        FailInput(enemyKind);
                    }
                }
            }

            if (willMove && !losed)
            {
                SetPosition(position + direction);
                playerBody.DORotateQuaternion(Quaternion.LookRotation(Vector2ToVector3(direction), Vector3.up), settings.animRotationTime).SetEase(settings.easeRotation);
            }

            hasChangedDirection = false;

            if (ticksStoppedNecrofargo >= 2)
            {
                playerAnim.SetTrigger(animDeathByNecro);
                Lose(DeathEnemySource.necrofargo);
            }
            if (ticksStoppedEscudo >= 2)
            {
                playerAnim.SetTrigger(animDeathByNecro);
                Lose(DeathEnemySource.escudo);
            }
            if (ticksStoppedMago >= 2)
            {
                playerAnim.SetTrigger(animDeathByNecro);
                Lose(DeathEnemySource.mago);
            }
            if (ticksStoppedTramposo >= 2)
            {
                playerAnim.SetTrigger(animDeathByNecro);
                Lose(DeathEnemySource.tramposo);
            }
            if (ticksStoppedMeteorito >= 2)
            {
                playerAnim.SetTrigger(animDeathByChoque);
                Lose(DeathEnemySource.meteorito);
            }
            if (ticksStoppedWater >= 2)
            {
                playerAnim.SetTrigger(animDeathByLago);
                Lose(DeathEnemySource.lago);
            }
            if (ticksStoppedDecoration >= 2)
            {
                playerAnim.SetTrigger(animDeathByChoque);
                Lose(DeathEnemySource.choque);
            }
        }
    }

    public void GetChicken()
    {
        var pollo = Instantiate(chicken, pivotForChicken);
        playerAnim.SetBool("withChicken", true);
        chickenOnHands = true;
    }
    public void FailInput(EnemyBehaviour.EnemyKind causeKind)
    {
        willMove = false;

        switch (causeKind)
        {
            case EnemyBehaviour.EnemyKind.Basic:
                ticksStoppedNecrofargo++;
                break;
            case EnemyBehaviour.EnemyKind.Trickster:
                ticksStoppedTramposo++;
                break;
            case EnemyBehaviour.EnemyKind.Shield:
                ticksStoppedEscudo++;
                break;
            case EnemyBehaviour.EnemyKind.Wizard:
                ticksStoppedMago++;
                break;
            case EnemyBehaviour.EnemyKind.SuperTrickster:
                ticksStoppedTramposo++;
                break;
            default:
                break;
        }
    }

    void SetPosition(Vector2 newPosition)
    {
        transform.DOMove(Vector2ToVector3(newPosition), Time.fixedDeltaTime).SetEase(settings.easeMovement);
        position = newPosition;
    }

    public Vector3 Vector2ToVector3(Vector2 position)
    {
        return new Vector3(position.x, 0, position.y);
    }

    public void ShieldHit()
    {
        playerAnim.SetTrigger(animDeathByNecro);
        Lose(DeathEnemySource.escudo);
    }

    void Lose(DeathEnemySource deathSource)
    {
        losed = true;
        DOTween.KillAll();
        uiManager.DeathAnim(deathSource);
        readyToStart = false;
        Invoke(nameof(LoadActualScene), 1.5f);
    }
    void LoadActualScene()
    {
        GameManager.instance.sceneTransitioner.TransitionToScene(SceneManager.GetActiveScene().name);
    }

    [Button]
    void Victory()
    {
        DOTween.KillAll();
        playerAnim.SetTrigger(animEnterBoat);
        levelEnded = true;
        readyToStart = false;

        if (GameManager.instance.currentLevelIndex >= 8)
        {
            chickenOnHands = false;
            if (!GameManager.instance.progress.watchedCinematics.Contains(Cinematics.postCerbero))
            {
                GameManager.instance.cinematicFromGameEnd = true;
                GameManager.instance.currentLevelIndex = 11;
                GameManager.instance.sceneTransitioner.TransitionToScene(cinematicScene);
            }
        }
        else
        {
            levelSceneController.OnWin();
            uiManager.WinAnim();
        }

        if (GameManager.instance.progress.unlockedLevels < GameManager.instance.player.levelSceneController.levelIndex)
        {
            GameManager.instance.progress.unlockedLevels += 1;
            GameManager.SaveProgress(GameManager.instance.progress);
        }
    }
}