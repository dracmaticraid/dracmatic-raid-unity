using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Level Config Assets", menuName = "Dracmatic Raid/Config Assets", order = 3)]
public class LevelConfigAssets : ScriptableObject
{
    public BiomeTilesData standardTiles;
    public BiomeTilesData meteoriteTiles;
    public BiomeTilesData templeTiles;

    public GameObject obstaclePrefab;
    public GameObject[] enemyPrefab;
    public GameObject startPrefab;
    public GameObject endPrefab;
    public GameObject meteoritePrefab;

    public Color obstacleColor;
    public Color decorationObstacleColor;
    public Color voidColor;
    public Color enemyColor;
    public Color enemyTricksterColor;
    public Color enemySperTricksterColor;
    public Color enemyShieldColor;
    public Color enemyArcherColor;
    public Color startColor;
    public Color endColor;

    [BoxGroup("Tile Color")]
    [BoxGroup("Tile Color/Tile TILE")]
    public Color floorColor;

    [BoxGroup("Tile Color/Tile Biome")]
    public Color standardBiomeColor;
    [BoxGroup("Tile Color/Tile Biome")]
    public Color meteoriteBiomeColor;
    [BoxGroup("Tile Color/Tile Biome")]
    public Color templeBiomeColor;

    [BoxGroup("Tile Color/Tile Side")]
    public Color zeroSidedTilesColor;
    [BoxGroup("Tile Color/Tile Side")]
    public Color oneSidedTilesColor;
    [BoxGroup("Tile Color/Tile Side")]
    public Color twoSidedTilesColor;
    [BoxGroup("Tile Color/Tile Side")]
    public Color twoCornerSidedTilesColor;
    [BoxGroup("Tile Color/Tile Side")]
    public Color treeSidedTilesColor;
    [BoxGroup("Tile Color/Tile Side")]
    public Color fourSidedTilesColor;

    [BoxGroup("Tile Color/Tile Rotation")]
    public Color upFacing;
    [BoxGroup("Tile Color/Tile Rotation")]
    public Color rightFacing;
    [BoxGroup("Tile Color/Tile Rotation")]
    public Color downFacing;
    [BoxGroup("Tile Color/Tile Rotation")]
    public Color leftFacing;

    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant0;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant1;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant2;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant3;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant4;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant5;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant6;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant7;
    [BoxGroup("Tile Color/Tile Variant")]
    public Color tileVariant8;

    [BoxGroup("Shield Orientation")]
    public Color[] customShieldOrientationColor;

    public int meteoriteFallTime;
    public int meteoriteDistanceTrigger;
    public int meteoriteDecomSpeed;
}
