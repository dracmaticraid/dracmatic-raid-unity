using UnityEngine;
using UnityEngine.UI;

public class OptionsMenu : MonoBehaviour
{
    public SwipeInputManager swipeInput;
    public GameObject buttonInput;

    public GameObject audioNegationImg;
    public GameObject sfxNegationImg;

    public Sprite[] swipeImg;
    public Sprite[] padImg;

    public Image swipeButton;
    public Image padButton;

    private bool isAudioOn = true;
    private bool isSfxOn = true;

    private void Start()
    {
        ClosePanel();
        switch (GameManager.instance.config.controlScheme)
        {
            case ControlSchemes.swipe:
                GoSwipe();
                break;
            case ControlSchemes.button:
                GoButton();
                break;
        }
    }

    public void GoSwipe()
    {
        if (swipeInput != null && buttonInput != null)
        {
            swipeInput.enabled = true;
            buttonInput.SetActive(false);
        }

        swipeButton.sprite = swipeImg[1];
        padButton.sprite = padImg[0];

        GameManager.instance.config.controlScheme = ControlSchemes.swipe;
    }
    public void GoButton()
    {
        if (swipeInput != null && buttonInput != null)
        {
            swipeInput.enabled = false;
            buttonInput.SetActive(true);
        }

        swipeButton.sprite = swipeImg[0];
        padButton.sprite = padImg[1];

        GameManager.instance.config.controlScheme = ControlSchemes.button;
    }

    public void ClosePanel()
    {
        Time.timeScale = 1;
        gameObject.SetActive(false);
    }

    public void sfxON()
    {
        isSfxOn = !isSfxOn;
        if (isSfxOn)
        {
            GameManager.instance.config.sfxEnabled = true;
            sfxNegationImg.SetActive(false);

        }
        else
        {
            GameManager.instance.config.sfxEnabled = false;
            sfxNegationImg.SetActive(true);
        }
    }
    public void audioON()
    {
        isAudioOn = !isAudioOn;
        if (isAudioOn)
        {
            GameManager.instance.config.musicEnabled = true;
            audioNegationImg.SetActive(false);

        }
        else
        {
            GameManager.instance.config.musicEnabled = false;
            audioNegationImg.SetActive(true);

        }
    }

    public void RewatchCinematics()
    {
        GameManager.instance.sceneTransitioner.TransitionToScene("RewatchCinematics");
    }
    public void RewatchTutorials()
    {
        GameManager.instance.sceneTransitioner.TransitionToScene("RewatchTutorials");
    }
}
