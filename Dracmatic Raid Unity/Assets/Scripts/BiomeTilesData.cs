using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Biome Tile Assets", menuName = "Dracmatic Raid/Biome Tile Assets", order = 5)]
public class BiomeTilesData : ScriptableObject
{
    public GameObject[] GetSidedTiles(LevelSpawner.SidedTile sidedTile)
    {
        switch (sidedTile)
        {
            default:
            case LevelSpawner.SidedTile.Zero:
                return zeroSided;
                break;
            case LevelSpawner.SidedTile.One:
                return oneSided;
                break;
            case LevelSpawner.SidedTile.Two:
                return twoSided;
                break;
            case LevelSpawner.SidedTile.TwoCorner:
                return twoCornerSided;
                break;
            case LevelSpawner.SidedTile.Tree:
                return treeSided;
                break;
            case LevelSpawner.SidedTile.Four:
                return fourSided;
                break;

        }
    } 

    public GameObject[] zeroSided;
    public GameObject[] oneSided;
    public GameObject[] twoSided;
    public GameObject[] twoCornerSided;
    public GameObject[] treeSided;
    public GameObject[] fourSided;
}
