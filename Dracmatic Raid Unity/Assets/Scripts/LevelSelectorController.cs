using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectorController : MonoBehaviour
{
    //public void Volver(){
    //    GameManager.instance.sceneTransitioner.TransitionToScene("MainMenu");
    //}
    private int lastLevel;

    public GameObject[] levelButtons;
    public Sprite[] levelSprites;

    public RectTransform container;
    public float[] containerYPos;

    public RectTransform posMarker;

    private void Start()
    {
        lastLevel = GameManager.instance.progress.unlockedLevels;
        for (int i = 0; i < levelButtons.Length; i++)
        {
            if (lastLevel == i)
            {
                levelButtons[i].GetComponent<Image>().sprite = levelSprites[1];
            }
            else if (lastLevel > i)
            {
                levelButtons[i].GetComponent<Image>().sprite = levelSprites[0];
            }
            else
            {
                levelButtons[i].GetComponent<Image>().sprite = levelSprites[2];
            }
        }
     
        if (lastLevel == 0)
        {
            posMarker.anchoredPosition = levelButtons[0].GetComponent<RectTransform>().anchoredPosition + new Vector2(0, 50);
            container.anchoredPosition = new Vector2(0, containerYPos[0]);
        }

        for (int i = 1; i < levelButtons.Length; i++)
        {
            if (lastLevel == i)
            {
                posMarker.anchoredPosition = levelButtons[i - 1].GetComponent<RectTransform>().anchoredPosition + new Vector2(0, 50);
                container.anchoredPosition = new Vector2(0, containerYPos[i - 1]);
            }
        }

        if (lastLevel > 8)
        {
            posMarker.anchoredPosition = levelButtons[8].GetComponent<RectTransform>().anchoredPosition + new Vector2(0, 50);
            container.anchoredPosition = new Vector2(0, containerYPos[8]);
        }
    }
    public void OnLevel(string levelName)
    {
        int nameIndex = Convert.ToInt32(levelName);
        if (GameManager.instance.progress.unlockedLevels >= nameIndex)
        {
            GameManager.instance.currentLevelIndex = nameIndex;
            if (GameManager.instance.levels[nameIndex].cinematic != null && (
                nameIndex == 0 && !GameManager.instance.progress.watchedCinematics.Contains(Cinematics.caronte)) ||
                nameIndex == 8 && !GameManager.instance.progress.watchedCinematics.Contains(Cinematics.preCerbero))
            {
                GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen("CinematicScene");
            }
            else
            {
                GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen(GameManager.instance.levels[nameIndex].levelName);
            }
        }
        else { Debug.Log("Level " + levelName + " Locked!"); }
    }

    public void OnDebugLevel(int index)
    {
        SceneManager.LoadScene(index);
    }

}
