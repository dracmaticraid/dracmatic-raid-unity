using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CerveroBehaviour : MonoBehaviour
{
    public Animator animator;
    bool isOnAtts = false;

    void Update(){
        if(!isOnAtts && GameManager.instance.player.readyToStart == true){
            isOnAtts = true;
            StartCoroutine(AttSequence());
        }
    }

    IEnumerator AttSequence(){
        Init();
        yield return new WaitForSeconds(1f);

        Init();
        yield return new WaitForSeconds(0.5f);

        Init();
        yield return new WaitForSeconds(2f);

        Init();
        yield return new WaitForSeconds(1f);

        Init();
        yield return new WaitForSeconds(0.3f);
    } 
    void Init(){
        animator.SetTrigger("cerveroAtt");
    }
}
