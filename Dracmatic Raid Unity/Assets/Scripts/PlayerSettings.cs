using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Settings", menuName = "Dracmatic Raid/Player Settings", order = 2)]
public class PlayerSettings : ScriptableObject
{
    public Ease easeMovement = Ease.Linear;
    public float animRotationTime = .15f;
    public Ease easeRotation = Ease.InOutQuad;

    public float rangeToParticle;
}
