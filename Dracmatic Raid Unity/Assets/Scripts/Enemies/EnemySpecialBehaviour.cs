using UnityEngine;

[RequireComponent(typeof(EnemyBehaviour))]
public class EnemySpecialBehaviour : MonoBehaviour
{
    public EnemyBehaviour baseBehaviour;
    void Awake()
    {
        baseBehaviour = GetComponent<EnemyBehaviour>();
    }

    public virtual void Interact()
    {
        baseBehaviour.Die();
    }

    public virtual void HitByWizard()
    {
        baseBehaviour.Die();
    }
    public virtual void HitByMeteorite()
    {
        baseBehaviour.Die();
    }
}
