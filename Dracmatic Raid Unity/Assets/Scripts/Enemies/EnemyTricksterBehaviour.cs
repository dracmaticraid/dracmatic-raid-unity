using UnityEngine;

public class EnemyTricksterBehaviour : EnemySpecialBehaviour
{
    bool didTheTrick = false;
    public GameObject fakeDeathPS;

    public bool isSuperTrickster = false;

    public override void Interact()
    {
        if (!didTheTrick)
            TrickMoment();
        else
            baseBehaviour.Die();
    }
    public override void HitByWizard()
    {
        if (!didTheTrick)
            TrickMoment();
        else
            baseBehaviour.Die();
    }

    public void TrickMoment()
    {
        GameObject fakeDeathPSGO = Instantiate(fakeDeathPS, transform.position, Quaternion.Euler(new Vector3(-90f, 0, 0)));
        Destroy(fakeDeathPSGO, 1f);

        GameManager.instance.currentLevel.SwipeObjectFromToWho(gameObject, baseBehaviour.newPos); //Matrix movement
        transform.position = new Vector3(baseBehaviour.newPos.x, transform.position.y, baseBehaviour.newPos.y); //InGame movement
        didTheTrick = true;
    }
}