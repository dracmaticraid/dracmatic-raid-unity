public class EnemyShieldBehaviour : EnemySpecialBehaviour
{
    public Orientations shieldFacing;

    public override void Interact()
    {
        // Shield facing player
        if (shieldFacing == GameManager.instance.player.inversePlayerOrientation)
        {
            GameManager.instance.player.ShieldHit();
        }
        else
        {
            baseBehaviour.Die(false);
        }
    }

    public override void HitByMeteorite()
    {
        baseBehaviour.Die(false);
    }

    public override void HitByWizard()
    {
        baseBehaviour.Die(false);
    }
}
