using System.Collections.Generic;
using UnityEngine;

public class ShieldTarget
{
    public GameObject targetObject;
    public Orientations orientations;

    public ShieldTarget(GameObject _targetObject, Orientations _orientations)
    {
        targetObject = _targetObject;
        orientations = _orientations;
    }
}

[RequireComponent(typeof(EnemyBehaviour))]
public class EnemyWizardBehaviour : EnemySpecialBehaviour
{
    public GameObject wizardBullet;
    private GameObject wizardBulletGO;

    public List<GameObject> toTopTiles;
    public List<GameObject> toDownTiles;
    public List<GameObject> toLeftTiles;
    public List<GameObject> toRightTiles;
    public List<ShieldTarget> onRangeShieldNecrofargos = new List<ShieldTarget>();

    public List<GameObject> laseredTiles;

    private Renderer renderer;

    private float currentTime;
    public float attCooldown;

    public Orientations inverseOrientation;

    private void Start()
    {
        renderer = GetComponentInChildren<Renderer>();

        for (int i = 0; i < GameManager.instance.currentLevel.levelTexture.height; i++)
        {
            var targetTopTile = GameManager.instance.currentLevel.GetTileAt(new Vector2(transform.position.x, transform.position.z + 1 + i));
            if (targetTopTile != null)
                toTopTiles.Add(targetTopTile);

            var targetDownTile = GameManager.instance.currentLevel.GetTileAt(new Vector2(transform.position.x, transform.position.z - 1 - i));
            if (targetDownTile != null)
                toDownTiles.Add(targetDownTile);

            var targetVerticalObject = GameManager.instance.currentLevel.GetObjectAt(new Vector2(transform.position.x, i));
            if (targetVerticalObject != null && targetVerticalObject.GetComponent<EnemyBehaviour>().enemyKind == EnemyBehaviour.EnemyKind.Shield)
            {
                ShieldTarget temp = new ShieldTarget(targetVerticalObject, targetVerticalObject.GetComponent<EnemyShieldBehaviour>().shieldFacing);
                onRangeShieldNecrofargos.Add(temp);
            }
        }

        for (int i = 0; i < GameManager.instance.currentLevel.levelTexture.width; i++)
        {
            var targetLeftTile = GameManager.instance.currentLevel.GetTileAt(new Vector2(transform.position.x + 1 + i, transform.position.z));
            if (targetLeftTile != null)
                toLeftTiles.Add(targetLeftTile);

            var targetRightTile = GameManager.instance.currentLevel.GetTileAt(new Vector2(transform.position.x - 1 - i, transform.position.z));
            if (targetRightTile != null)
                toRightTiles.Add(targetRightTile);

            var targetHorizontalObject = GameManager.instance.currentLevel.GetObjectAt(new Vector2(i, transform.position.z));
            if (targetHorizontalObject != null && targetHorizontalObject.GetComponent<EnemyBehaviour>().enemyKind == EnemyBehaviour.EnemyKind.Shield)
            {
                ShieldTarget temp = new ShieldTarget(targetHorizontalObject, targetHorizontalObject.GetComponent<EnemyShieldBehaviour>().shieldFacing);
                onRangeShieldNecrofargos.Add(temp);
            }

        }
    }

    private void Rotate(Orientations ori, Vector3 rot)
    {
        transform.rotation = Quaternion.Euler(rot);
        inverseOrientation = ori;
    }

    private void Update()
    {
        if (baseBehaviour.isDead)
            return;

        currentTime += Time.deltaTime;

        if (!renderer.isVisible)
            return;

        if (currentTime >= attCooldown)
        {
            if (baseBehaviour.player.GetComponent<PlayerController>().furtherTile != null)
            {
                for (int i = 0; i < toTopTiles.Count; i++)
                {
                    if (baseBehaviour.player.GetComponent<PlayerController>().furtherTile.transform.position == toTopTiles[i].transform.position)
                    {
                        Rotate(Orientations.Top, Vector3.zero);
                        wizardBulletGO = Instantiate(wizardBullet, transform);
                        ShootDirection(toTopTiles);
                        Destroy(wizardBulletGO, GameManager.instance.levelController.reverseCorruptionTime);
                        laseredTiles.Clear();
                    }
                }

                for (int i = 0; i < toDownTiles.Count; i++)
                {
                    if (baseBehaviour.player.GetComponent<PlayerController>().furtherTile.transform.position == toDownTiles[i].transform.position)
                    {
                        Rotate(Orientations.Down, new Vector3(0, 180, 0));
                        wizardBulletGO = Instantiate(wizardBullet, transform);
                        ShootDirection(toDownTiles);
                        Destroy(wizardBulletGO, GameManager.instance.levelController.reverseCorruptionTime);
                        laseredTiles.Clear();
                    }
                }

                for (int i = 0; i < toRightTiles.Count; i++)
                {
                    if (baseBehaviour.player.GetComponent<PlayerController>().furtherTile.transform.position == toRightTiles[i].transform.position)
                    {
                        Rotate(Orientations.Right, new Vector3(0, -90, 0));
                        wizardBulletGO = Instantiate(wizardBullet, transform);
                        ShootDirection(toRightTiles);
                        Destroy(wizardBulletGO, GameManager.instance.levelController.reverseCorruptionTime);
                        laseredTiles.Clear();
                    }
                }

                for (int i = 0; i < toLeftTiles.Count; i++)
                {
                    if (baseBehaviour.player.GetComponent<PlayerController>().furtherTile.transform.position == toLeftTiles[i].transform.position)
                    {
                        Rotate(Orientations.Left, new Vector3(0, 90, 0));
                        wizardBulletGO = Instantiate(wizardBullet, transform);
                        ShootDirection(toLeftTiles);
                        Destroy(wizardBulletGO, GameManager.instance.levelController.reverseCorruptionTime);
                        laseredTiles.Clear();
                    }
                }
            }
        }
    }

    private void ShootDirection(List<GameObject> directionToSwipe)
    {

        currentTime = 0;
        for (int i = 0; i < directionToSwipe.Count; i++)
        {
            for (int j = 0; j < onRangeShieldNecrofargos.Count; j++)
            {
                if (onRangeShieldNecrofargos[j].targetObject != null)
                {
                    if (directionToSwipe[i].transform.position == onRangeShieldNecrofargos[j].targetObject.transform.position)
                    {
                        if (onRangeShieldNecrofargos[j].targetObject.GetComponent<EnemyBehaviour>().shieldOnGuard && onRangeShieldNecrofargos[j].orientations == inverseOrientation)
                            return;
                    }
                }
            }
            GameManager.instance.levelController.SwitchTileToLasered(GameManager.Get2DGridPosition(directionToSwipe[i].transform.position));

            laseredTiles.Add(GameManager.instance.currentLevel.GetTileAt(directionToSwipe[i].transform.position));

            var targetObject = GameManager.instance.currentLevel.GetObjectAt(GameManager.Get2DGridPosition(directionToSwipe[i].transform.position));
            if (targetObject != null)
            {
                targetObject.SendMessage("HitByWizard");
            }
        }
    }

    public override void Interact()
    {
        baseBehaviour.Die(false);
        Destroy(wizardBulletGO);

        foreach (var item in laseredTiles)
        {
            GameManager.instance.levelController.SwitchTileToBasic(item.transform.position);
        }
        laseredTiles.Clear();
    }
    public override void HitByMeteorite()
    {
        baseBehaviour.Die(false);
    }
    public override void HitByWizard()
    {
        baseBehaviour.Die(false);
    }

}

