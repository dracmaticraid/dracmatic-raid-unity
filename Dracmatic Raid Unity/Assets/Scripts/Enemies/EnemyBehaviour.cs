using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    public enum EnemyKind { Basic, Trickster, Shield, Wizard, SuperTrickster };
    public EnemyKind enemyKind;

    [Header("Trickster variables")]
    public Vector2 newPos;
    [Header("Shield variables")]
    public float enemyShieldGuardRange;
    [Header("Shield variables")]
    public GameObject shield;
    [Header("Super Trickster variables")]
    public Vector2[] superTricksterNewPoses;

    public Animator enemyAnimator;
    public GameObject hitParticlesOut;
    public GameObject hitParticlesInn;
    public Transform player;

    public PlayerSettings playerSetting;

    public GameObject enemyDeathPS;

    public Renderer meshRenderer;

    public bool isDead = false;
    private bool isOutParticleScaled;
    public bool shieldOnGuard;
    private float additiveFloat;

    void Start()
    {
        player = GameManager.instance.player.transform;
        enemyAnimator = gameObject.GetComponent<Animator>();
        float i = Random.Range(0.80f, 1.2f);
        enemyAnimator.speed *= i;

        hitParticlesOut.SetActive(false);
        hitParticlesOut.transform.localScale = Vector3.zero;
        hitParticlesInn.SetActive(false);
        hitParticlesInn.transform.localScale = Vector3.zero;
    }

    [Button]
    public void Die(bool isWithDracmas = true)
    {
        isDead = true;

        GameObject explosionPSGO = Instantiate(enemyDeathPS, transform.position, Quaternion.Euler(new Vector3(-90f, 0, 0)));
        Destroy(explosionPSGO, 1f);

        if (isWithDracmas)
        {
            GameManager.instance.player.enemiesDefeated++;

            if (!GameManager.instance.player.isPowered)
            {
                GameManager.instance.player.isPowered = true;
                GameManager.instance.player.meshRenderer.material.SetFloat("_isPulsing", 1);

            }
        }

        hitParticlesOut.transform.DOScale(0, 0.1f);
        hitParticlesInn.transform.DOScale(0, 0.1f);

        enemyAnimator.SetTrigger("NecroDie");

        transform.DOScale(0, 1.5f).SetEase(Ease.InCubic);
        Destroy(gameObject, 2f);
    }

    void Update()
    {
        if (isDead)
        {
            additiveFloat += Time.deltaTime;
            meshRenderer.material.SetFloat("_DisolveThreshold", additiveFloat);
            return;
        }

        float distance = Vector3.Distance(transform.position, player.position);
        if (distance < playerSetting.rangeToParticle)
        {
            hitParticlesOut.SetActive(true);
            if (!isOutParticleScaled)
            {
                hitParticlesOut.transform.DOScale(1, 0.1f);
                isOutParticleScaled = true;
            }
        }
        else
        {
            if (isOutParticleScaled)
            {
                hitParticlesOut.transform.DOScale(0, 0.1f);
                isOutParticleScaled = false;
            }
            hitParticlesOut.SetActive(false);
        }

        if (distance < playerSetting.rangeToParticle)
        {
            hitParticlesInn.SetActive(true);
            float lerpedDist = Mathf.Lerp(0, 1.3f, (playerSetting.rangeToParticle - distance) / playerSetting.rangeToParticle);
            hitParticlesInn.transform.localScale = new Vector3(lerpedDist, lerpedDist, lerpedDist);
        }

        if (enemyKind == EnemyKind.Shield)
        {
            if (distance <= enemyShieldGuardRange)
            {
                shieldOnGuard = true;
                //Set Animation (bool on guard)
            }
            else
            {
                shieldOnGuard = false;
                //Set Animation (!bool on guard)
            }
        }
    }

    public void HitByDistance()
    {
        if (!shieldOnGuard)
            Die();
    }
}
