using Sirenix.OdinInspector;
using UnityEngine;

public class LevelSpawner : MonoBehaviour
{
    public LevelConfigAssets configAssets;
    public LevelData level;
    public Transform tilesContainer;
    public Transform enemiesContainer;
    public Transform specialTilesContainer;
    public Transform meteoritesContainer;
    public GameObject meteoritePS;

    private int tricksterIndex = 0;
    private int shieldIndex = 0;

    public enum BiomeTile { Standard, Meteorite, Temple }
    public enum SidedTile { Zero, One, Two, TwoCorner, Tree, Four }
    public enum TileTag { Obstacle, Decoration, BasicTile }

    void Awake()
    {
        GameManager.instance.currentLevel = level;
    }

    // Gets called on scene start from GameManager
    [Button]
    public void SpawnLevel()
    {
        ResetLevel();

        if (level.shieldRotationFromMap)
            level.ShieldRotationMapToData();

        int width = level.levelTexture.width;
        int height = level.levelTexture.height;
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color pixelColor = level.levelTexture.GetPixel(x, y);

                // Floor tile
                if (pixelColor == configAssets.floorColor)
                {
                    TileDeterminer(x, y, TileTag.BasicTile);

                    continue;
                }
                else if (pixelColor == configAssets.obstacleColor)
                {
                    TileDeterminer(x, y, TileTag.Obstacle);
                    continue;
                }
                else if (pixelColor == configAssets.decorationObstacleColor)
                {
                    TileDeterminer(x, y, TileTag.Decoration);
                    continue;
                }
                else if (pixelColor == configAssets.enemyColor)
                {
                    SpawnEnemy(x, y, EnemyBehaviour.EnemyKind.Basic);
                    continue;
                }
                else if (pixelColor == configAssets.enemyTricksterColor)
                {
                    SpawnEnemy(x, y, EnemyBehaviour.EnemyKind.Trickster);
                    continue;
                }
                else if (pixelColor == configAssets.enemyShieldColor)
                {
                    SpawnEnemy(x, y, EnemyBehaviour.EnemyKind.Shield);
                    continue;
                }
                else if (pixelColor == configAssets.enemyArcherColor)
                {
                    SpawnEnemy(x, y, EnemyBehaviour.EnemyKind.Wizard);
                    continue;
                }
                else if (pixelColor == configAssets.enemySperTricksterColor)
                {
                    SpawnEnemy(x, y, EnemyBehaviour.EnemyKind.SuperTrickster);
                    continue;
                }
                else if (pixelColor == configAssets.startColor)
                {
                    SpawnStartTile(x, y);
                    continue;
                }
                else if (pixelColor == configAssets.endColor)
                {
                    SpawnEndTile(x, y);
                }

            }
        }

        for (int i = 0; i < level.meteorites.Count; i++)
        {
            GameObject met = Instantiate(configAssets.meteoritePrefab, new Vector3(level.meteorites[i].x, 0, level.meteorites[i].y), Quaternion.identity);
            MeteoriteBehaviour meteoriteBehaviour = met.GetComponent<MeteoriteBehaviour>();
            meteoriteBehaviour.spawnPos = level.meteorites[i];
            met.transform.parent = meteoritesContainer;
        }
    }

    public void TileDeterminer(int x, int y, TileTag tileTag)
    {
        Color biomePixelColor = level.biomeLevelTexture.GetPixel(x, y);
        Color tilePrefabPixelColor = level.tileKindLevelTexture.GetPixel(x, y);
        Color tileRotationPixelColor = level.tileRotationLevelTexture.GetPixel(x, y);
        Color tileSpecificKindLevelTexture = level.tileSpecificKindLevelTexture.GetPixel(x, y);

        var tilePrefab = GetTilePrefab(SetTileBiome(biomePixelColor), SetTilePrefab(tilePrefabPixelColor), SetTileVariantIndex(tileSpecificKindLevelTexture));

        SpawnTile(x, y, tilePrefab, SetTileRotation(tileRotationPixelColor), tileTag);
    }
    public BiomeTile SetTileBiome(Color biomePixelColor)
    {
        if (biomePixelColor == configAssets.standardBiomeColor)
            return BiomeTile.Standard;
        else if (biomePixelColor == configAssets.meteoriteBiomeColor)
            return BiomeTile.Meteorite;
        else
            return BiomeTile.Temple;
    }
    public SidedTile SetTilePrefab(Color tilePrefabPixelColor)
    {
        if (tilePrefabPixelColor == configAssets.zeroSidedTilesColor)
            return SidedTile.Zero;
        else if (tilePrefabPixelColor == configAssets.oneSidedTilesColor)
            return SidedTile.One;
        else if (tilePrefabPixelColor == configAssets.twoSidedTilesColor)
            return SidedTile.Two;
        else if (tilePrefabPixelColor == configAssets.twoCornerSidedTilesColor)
            return SidedTile.TwoCorner;
        else if (tilePrefabPixelColor == configAssets.treeSidedTilesColor)
            return SidedTile.Tree;
        else
            return SidedTile.Four;
    }
    public float SetTileRotation(Color tileRotationPixelColor)
    {
        if (tileRotationPixelColor == configAssets.upFacing)
            return 180;
        else if (tileRotationPixelColor == configAssets.rightFacing)
            return 270;
        else if (tileRotationPixelColor == configAssets.downFacing)
            return 0;
        else if ((tileRotationPixelColor == configAssets.leftFacing))
            return 90;
        else
        {
            var random = Random.Range(0, 4);
            switch (random)
            {
                default:
                case 0:
                    return 180;
                    break;
                case 1:
                    return 270;
                    break;
                case 2:
                    return 0;
                    break;
                case 3:
                    return 90;
                    break;
            }
        }

    }
    public int SetTileVariantIndex(Color tileSpecificKindLevelTexture)
    {
        if (tileSpecificKindLevelTexture == configAssets.tileVariant0)
            return 0;
        else if (tileSpecificKindLevelTexture == configAssets.tileVariant1)
            return 1;
        else if (tileSpecificKindLevelTexture == configAssets.tileVariant2)
            return 2;
        else if (tileSpecificKindLevelTexture == configAssets.tileVariant3)
            return 3;
        else if (tileSpecificKindLevelTexture == configAssets.tileVariant4)
            return 4;
        else if (tileSpecificKindLevelTexture == configAssets.tileVariant5)
            return 5;
        else if (tileSpecificKindLevelTexture == configAssets.tileVariant6)
            return 6;
        else if ((tileSpecificKindLevelTexture == configAssets.tileVariant7))
            return 7;
        else
            return 8;

    }
    public GameObject GetTilePrefab(BiomeTile biomeTile, SidedTile sidedTile, int variantIndex)
    {
        switch (biomeTile)
        {
            default:
            case BiomeTile.Standard:
                return configAssets.standardTiles.GetSidedTiles(sidedTile)[Random.Range(0, configAssets.standardTiles.GetSidedTiles(sidedTile).Length)];
                break;
            case BiomeTile.Meteorite:
                return configAssets.meteoriteTiles.GetSidedTiles(sidedTile)[Random.Range(0, configAssets.meteoriteTiles.GetSidedTiles(sidedTile).Length)];
                break;
            case BiomeTile.Temple:
                return configAssets.templeTiles.GetSidedTiles(sidedTile)[variantIndex];
                break;
        }
    }

    void SpawnTile(int x, int y, GameObject tilePrefab, float rotationTile, TileTag tag)
    {
        Vector3 xyPosition = GameManager.GetXYWorldPosition(x, y);
        GameObject tile = Instantiate(tilePrefab, xyPosition, Quaternion.Euler(Vector3.up * rotationTile));

        tile.tag = tag.ToString();
        tile.transform.parent = tilesContainer;
        level.tilesLayer.Add(new Vector2(x, y), tile);
    }

    void SpawnStartTile(int x, int y)
    {
        Vector3 xyPosition = GameManager.GetXYWorldPosition(x, y);
        GameObject startTile = Instantiate(configAssets.startPrefab, xyPosition, Quaternion.Euler(new Vector3(0, -90, 0)));
        startTile.transform.parent = specialTilesContainer;
        level.tilesLayer.Add(new Vector2(x, y), startTile);
        level.startPosition = new Vector2(x, y);
    }
    void SpawnEndTile(int x, int y)
    {
        Vector3 xyPosition = GameManager.GetXYWorldPosition(x, y);
        GameObject endTile = Instantiate(configAssets.endPrefab, xyPosition, Quaternion.identity);
        endTile.transform.parent = specialTilesContainer;
        level.tilesLayer.Add(new Vector2(x, y), endTile);
        level.endPosition = new Vector2(x, y);
    }

    void SpawnEnemy(int x, int y, EnemyBehaviour.EnemyKind kind)
    {
        Vector3 xyPosition = GameManager.GetXYWorldPosition(x, y);

        //Spawn Enemy
        GameObject enemy = Instantiate(configAssets.enemyPrefab[(int)kind], xyPosition, Quaternion.Euler(0, 180, 0));
        switch (kind)
        {
            case EnemyBehaviour.EnemyKind.Basic:
                enemy.GetComponent<EnemyBehaviour>().enemyKind = EnemyBehaviour.EnemyKind.Basic;
                enemy.AddComponent<EnemyBasicBehaviour>();
                break;
            case EnemyBehaviour.EnemyKind.Trickster:
                enemy.GetComponent<EnemyBehaviour>().enemyKind = EnemyBehaviour.EnemyKind.Trickster;
                enemy.GetComponent<EnemyBehaviour>().newPos = level.tricksterEnemyEndPos[tricksterIndex];
                tricksterIndex++;
                break;
            case EnemyBehaviour.EnemyKind.SuperTrickster:
                enemy.GetComponent<EnemyBehaviour>().enemyKind = EnemyBehaviour.EnemyKind.SuperTrickster;
                enemy.GetComponent<EnemyBehaviour>().superTricksterNewPoses = level.superTricksterEnemyPositions;
                break;
            case EnemyBehaviour.EnemyKind.Shield:
                enemy.GetComponent<EnemyBehaviour>().enemyKind = EnemyBehaviour.EnemyKind.Shield;
                enemy.AddComponent<EnemyShieldBehaviour>();
                switch (level.shieldFacing[shieldIndex])
                {
                    case Orientations.Top:
                        enemy.transform.rotation = Quaternion.Euler(0, 180, 0);
                        enemy.GetComponent<EnemyShieldBehaviour>().shieldFacing = Orientations.Top;
                        break;
                    case Orientations.Right:
                        enemy.transform.rotation = Quaternion.Euler(0, 90, 0);
                        enemy.GetComponent<EnemyShieldBehaviour>().shieldFacing = Orientations.Right;
                        break;
                    case Orientations.Down:
                        enemy.transform.rotation = Quaternion.Euler(0, 0, 0);
                        enemy.GetComponent<EnemyShieldBehaviour>().shieldFacing = Orientations.Down;
                        break;
                    case Orientations.Left:
                        enemy.transform.rotation = Quaternion.Euler(0, -90, 0);
                        enemy.GetComponent<EnemyShieldBehaviour>().shieldFacing = Orientations.Left;
                        break;
                }
                shieldIndex++;
                break;
            case EnemyBehaviour.EnemyKind.Wizard:
                enemy.GetComponent<EnemyBehaviour>().enemyKind = EnemyBehaviour.EnemyKind.Wizard;
                break;
        }
        enemy.transform.parent = enemiesContainer;
        level.objectsLayer.Add(new Vector2(x, y), enemy);

        TileDeterminer(x, y, TileTag.BasicTile);
    }

    [Button]
    void ResetLevel()
    {
        for (int i = tilesContainer.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(tilesContainer.GetChild(i).gameObject);
        }
        for (int i = enemiesContainer.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(enemiesContainer.GetChild(i).gameObject);
        }
        for (int i = specialTilesContainer.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(specialTilesContainer.GetChild(i).gameObject);
        }
        for (int i = meteoritesContainer.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(meteoritesContainer.GetChild(i).gameObject);
        }

        level.tilesLayer.Clear();
        level.objectsLayer.Clear();

        tricksterIndex = 0;
        shieldIndex = 0;
    }
}
