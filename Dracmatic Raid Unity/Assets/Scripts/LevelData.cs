using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Level Data", menuName = "Dracmatic Raid/Level Data", order = 1)]
public class LevelData : SerializedScriptableObject
{
    public int numberOfDracmasToCompleteLevel;

    public Texture2D levelTexture;
    public Texture2D biomeLevelTexture;
    public Texture2D tileKindLevelTexture;
    public Texture2D tileSpecificKindLevelTexture;
    public Texture2D tileRotationLevelTexture;



    public List<Vector2> meteorites = new List<Vector2>();

    [HideInInspector]
    public Dictionary<Vector2, GameObject> tilesLayer = new Dictionary<Vector2, GameObject>();
    [HideInInspector]
    public Dictionary<Vector2, GameObject> objectsLayer = new Dictionary<Vector2, GameObject>();

    public float journeyTime;

    public float panTime;
    public Vector3[] panPoints;

    public Vector2 startDirection;
    public Vector2 startPosition = Vector2.up;
    public Vector2 endPosition;

    public Vector2[] tricksterEnemyEndPos;

    public bool isThereASuperTrickster = false;
    [ShowIf(nameof(isThereASuperTrickster))]
    public Vector2[] superTricksterEnemyPositions;

    [BoxGroup("ShieldOrientation")]
    public bool shieldRotationFromMap;
    [BoxGroup("ShieldOrientation")]
    [ShowIf(nameof(shieldRotationFromMap))]
    public Texture2D shieldRotationMap = null;
    [BoxGroup("ShieldOrientation")]
    [ShowIf(nameof(shieldRotationFromMap))]
    public Color[] customShieldOrientationColor;
    [BoxGroup("ShieldOrientation")]
    public Orientations[] shieldFacing;

    public GameObject GetTileAt(Vector2 position)
    {
        GameObject tile = null;
        tilesLayer.TryGetValue(position, out tile);

        return tile;
    }

    public GameObject GetObjectAt(Vector2 position)
    {
        GameObject objectOnTile = null;
        objectsLayer.TryGetValue(position, out objectOnTile);

        return objectOnTile;
    }

    public void SwipeObjectFromToWho(GameObject gameObject, Vector2 destiny)
    {
        objectsLayer.Remove(GameManager.Get2DPosition(gameObject.transform.position));
        objectsLayer.Add(destiny, gameObject);
    }

    [BoxGroup("ShieldOrientation")]
    [ShowIf(nameof(shieldRotationFromMap))]
    [Button("Shield Rotation From Map To Data")]
    public void ShieldRotationMapToData()
    {
        int width = shieldRotationMap.width;
        int height = shieldRotationMap.height;
        int shieldIndex = 0;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                Color pixelColor = shieldRotationMap.GetPixel(x, y);

                if (pixelColor == customShieldOrientationColor[0])
                {
                    shieldFacing[shieldIndex] = Orientations.Down;
                    shieldIndex++;
                }
                else if (pixelColor == customShieldOrientationColor[1])
                {
                    shieldFacing[shieldIndex] = Orientations.Right;
                    shieldIndex++;
                }
                else if (pixelColor == customShieldOrientationColor[2])
                {
                    shieldFacing[shieldIndex] = Orientations.Top;
                    shieldIndex++;
                }
                else if (pixelColor == customShieldOrientationColor[3])
                {
                    shieldFacing[shieldIndex] = Orientations.Left;
                    shieldIndex++;
                }

                //Debug.Log("EnemyIndex: " + shieldIndex + "; Facing: " + shieldFacing[shieldIndex]);

            }
        }
    }
}