using TMPro;
using UnityEngine;

[System.Serializable]
public struct SubtitleData
{
    public float time;
    public string text;
}
public class CinematicSubtitles : MonoBehaviour
{
    public SubtitleData[] subtitles;
    public TextMeshProUGUI textField;

    public CinematicManager cinematicManager;

    private float generalTime = 0;
    private int nextSubtitleIndex = 0;

    private void Start()
    {
        cinematicManager = GetComponentInParent<CinematicManager>();
    }
    private void Update()
    {
        generalTime += Time.deltaTime;

        if (nextSubtitleIndex < subtitles.Length && generalTime > subtitles[nextSubtitleIndex].time)
        {
            textField.text = subtitles[nextSubtitleIndex].text;
            nextSubtitleIndex++;
        }
    }

    public void SkipCinematic()
    {
        GetComponentInParent<CinematicManager>().ReturnToPlayScene();
    }
}
