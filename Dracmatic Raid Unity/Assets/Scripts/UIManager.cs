using DG.Tweening;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Image deathDisplay;
    public GameObject deathAnimContainer;
    public DeathAnimationsData animationsData;
    public GameObject winPanel;
    public TextMeshProUGUI dracmasCount;

    public enum DeathEnemySource { lago, choque, corrupcion, escudo, laser, mago, meteorito, necrofargo, pobre, tramposo }

    void Start()
    {
        deathDisplay = deathAnimContainer.transform.GetChild(0).GetComponent<Image>();

        deathAnimContainer.SetActive(false);
        winPanel.SetActive(false);
        AddDracmasToUI();
    }

    public void AddDracmasToUI()
    {
        dracmasCount.transform.parent.DOShakeScale(0.3f, 1);
        dracmasCount.text = GameManager.instance.player.enemiesDefeated + " / " + GameManager.instance.currentLevel.numberOfDracmasToCompleteLevel;
    }

    public void DeathAnim(DeathEnemySource deathSource)
    {
        Time.timeScale = 1f;
        deathAnimContainer.SetActive(true);
        deathAnimContainer.gameObject.GetComponent<Image>().DOFade(1f, 0.5f);

        DeathKind(deathSource);
    }

    public void DeathKind(DeathEnemySource deathSource)
    {
        switch (deathSource)
        {
            case DeathEnemySource.lago:
                StartCoroutine(ImageSuccession(animationsData.caidaLago));
                break;
            case DeathEnemySource.choque:
                StartCoroutine(ImageSuccession(animationsData.genericChoque));
                break;
            case DeathEnemySource.corrupcion:
                StartCoroutine(ImageSuccession(animationsData.corrupcion));
                break;
            case DeathEnemySource.escudo:
                StartCoroutine(ImageSuccession(animationsData.escudo));
                break;
            case DeathEnemySource.laser:
                StartCoroutine(ImageSuccession(animationsData.laser));
                break;
            case DeathEnemySource.mago:
                StartCoroutine(ImageSuccession(animationsData.mago));
                break;
            case DeathEnemySource.meteorito:
                StartCoroutine(ImageSuccession(animationsData.meteorito));
                break;
            case DeathEnemySource.necrofargo:
                StartCoroutine(ImageSuccession(animationsData.necrofargo));
                break;
            case DeathEnemySource.pobre:
                StartCoroutine(ImageSuccession(animationsData.pobre));
                break;
            case DeathEnemySource.tramposo:
                StartCoroutine(ImageSuccession(animationsData.tramposo));
                break;
        }
    }

    IEnumerator ImageSuccession(Sprite[] imgs)
    {
        for (int i = 0; i < imgs.Length; i++)
        {
            deathDisplay.sprite = imgs[i];
            yield return new WaitForSeconds(animationsData.timeInterImages);
        }
    }
    public void WinAnim()
    {
        Time.timeScale = 1f;
        winPanel.SetActive(true);
        winPanel.gameObject.GetComponent<Image>().DOFade(1f, 0.5f);
    }

    public void InputPanel()
    {
        Time.timeScale = 0f;
    }
}
