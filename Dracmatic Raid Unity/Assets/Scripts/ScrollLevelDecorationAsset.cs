using UnityEngine;

public class ScrollLevelDecorationAsset : MonoBehaviour
{
    public float InnerPos, OuterPos;
    public float decoMinPos;
    public float decoPos;
    public float decoMaxPos;


    private RectTransform rectTransform;
    private ScrollLevelDecorationBehaviour scrollLevelDecorationBehaviour;
    public bool isCloud;

    void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        scrollLevelDecorationBehaviour = FindObjectOfType<ScrollLevelDecorationBehaviour>();
        decoPos = rectTransform.anchoredPosition.y;
    }
    void Update()
    {
        if (GameManager.instance.progress.unlockedLevels >= 9)
            rectTransform.localPosition = new Vector2(scrollLevelDecorationBehaviour.LerpFuntionCalculator(InnerPos, OuterPos), rectTransform.localPosition.y);
    }
}


