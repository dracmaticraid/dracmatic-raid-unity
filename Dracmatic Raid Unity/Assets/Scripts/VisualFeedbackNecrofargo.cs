using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualFeedbackNecrofargo : MonoBehaviour
{
    public float distance;
    public Transform filler;

    [Range(2f, 10f)]
    public float sliderFloat;
    [Range(0f, 1f)]
    public float fillerScale;

    private void Start() {
        filler.transform.localScale = Vector3.zero;
    }

}
