using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    public class TileData
    {
        public bool isObstacle;
        public MeshRenderer rend;
    }
    public Dictionary<Vector2, TileData> tiles = new Dictionary<Vector2, TileData>();

    public LevelSpawner levelSpawner;
    private LevelData levelData;

    public float reverseCorruptionTime = 1f;

    void Awake()
    {
        levelSpawner = gameObject.GetComponent<LevelSpawner>();
    }
    public void Start()
    {
        GameManager.instance.levelController = this;
        levelSpawner.SpawnLevel();
        levelData = GameManager.instance.currentLevel;

        switch (GameManager.instance.currentLevelIndex)
        {
            case 2:
                if (GameManager.instance.progress.watchedTutorials == 1 || GameManager.instance.commingFromRewatchTutorials)
                {
                    GameManager.instance.commingFromRewatchTutorials = false;
                    //Show Tuto 2 Pop-Up
                }
                break;
            case 4:
                if (GameManager.instance.progress.watchedTutorials == 2 || GameManager.instance.commingFromRewatchTutorials)
                {
                    GameManager.instance.commingFromRewatchTutorials = false;
                    //Show Tuto 3 Pop-Up
                }
                break;
            case 6:
                if (GameManager.instance.progress.watchedTutorials == 3 || GameManager.instance.commingFromRewatchTutorials)
                {
                    GameManager.instance.commingFromRewatchTutorials = false;
                    //Show Tuto 4 Pop-Up
                }
                break;
        }

        Init();
    }

    void Init()
    {
        foreach (var tile in levelData.tilesLayer)
        {
            TileData tileData = new TileData();
            tileData.isObstacle = tile.Value.CompareTag("Obstacle");
            tileData.rend = tile.Value.GetComponentInChildren<MeshRenderer>();

            tiles.Add(tile.Key, tileData);
        }
        FindPlayer();
    }

    void FindPlayer()
    {
        GameManager.instance.player.StartLevel();
    }

    public void SwitchTileToObstacle(Vector2 position)
    {
        GameObject tile = levelData.GetTileAt(position);
        tile.tag = "Obstacle";
        TileData tileData = tiles[position];
        tileData.isObstacle = true;

        StartCoroutine(SwitchBackTile(position));
    }
    public void SwitchTileToLasered(Vector2 position)
    {
        GameObject tile = levelData.GetTileAt(position);
        tile.tag = "Lasered";
        TileData tileData = tiles[position];
        tileData.isObstacle = true;

        StartCoroutine(SwitchBackTile(position));
    }
    IEnumerator SwitchBackTile(Vector2 position)
    {
        yield return new WaitForSeconds(reverseCorruptionTime);
        SwitchTileToBasic(position);
    }

    public void SwitchTileToBasic(Vector2 position)
    {
        GameObject tile = levelData.GetTileAt(position);
        tile.tag = "BasicTile";
        TileData tileData = tiles[position];
        tileData.isObstacle = false;
    }
}


