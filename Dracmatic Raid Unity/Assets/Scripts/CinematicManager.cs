using UnityEngine;
using UnityEngine.Video;

public class CinematicManager : MonoBehaviour
{
    private VideoPlayer videoPlayer;

    void Start()
    {
        Instantiate(GameManager.instance.levels[GameManager.instance.currentLevelIndex].cinematic, transform);

        videoPlayer = GetComponent<VideoPlayer>();
        videoPlayer.loopPointReached += CinematicEnded;
    }
    private void CinematicEnded(VideoPlayer videoPlayer)
    {
        ReturnToPlayScene();
    }

    public void ReturnToPlayScene()
    {
        if (GameManager.instance.cinematicFromRewatch)
        {
            GameManager.instance.cinematicFromRewatch = false;
            GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen("RewatchCinematics");
        }
        else if (GameManager.instance.cinematicFromIntro)
        {
            GameManager.instance.cinematicFromIntro = false;
            GameManager.instance.progress.watchedCinematics.Add(Cinematics.intro);
            GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen("Introduction");
        }
        else if (GameManager.instance.cinematicFromGameEnd)
        {
            GameManager.instance.cinematicFromGameEnd = false;
            GameManager.instance.progress.watchedCinematics.Add(Cinematics.postCerbero);

            GameManager.instance.cinematicFromFinish = true;
            GameManager.instance.currentLevelIndex = 12;
            GameManager.instance.sceneTransitioner.TransitionToScene("CinematicScene");
        }
        else if (GameManager.instance.cinematicFromFinish)
        {
            GameManager.instance.cinematicFromFinish = false;
            GameManager.instance.progress.watchedCinematics.Add(Cinematics.creditos);
            GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen("Introduction");
        }
        else
        {
            if (GameManager.instance.currentLevelIndex == 0)
                GameManager.instance.progress.watchedCinematics.Add(Cinematics.caronte);
            else
                GameManager.instance.progress.watchedCinematics.Add(Cinematics.preCerbero);

            GameManager.instance.sceneTransitioner.TransitionWithLoadingScreen(GameManager.instance.levels[GameManager.instance.currentLevelIndex].levelName);
        }

        GameManager.SaveProgress(GameManager.instance.progress);
    }
}
