using DG.Tweening;
using System.Collections.Generic;
using UnityEngine;


public class CaronteBehaviour : MonoBehaviour
{
    LevelData levelData;
    public Animator animator;
    private bool isOnJourney = false;
    bool levelComplete = false;

    public List<Vector3> v4JourneyPoints;

    void Start()
    {
        levelData = GameManager.instance.currentLevel;
        animator = GetComponentInChildren<Animator>();
        GameManager.instance.caronteOnEndPos = false;

        v4JourneyPoints.Clear();

        Transform carontePoints = GameObject.FindGameObjectWithTag("CarontePoints").transform;
        for (int i = 0; i < carontePoints.childCount; i++)
        {
            v4JourneyPoints.Add(carontePoints.GetChild(i).transform.position);
        }
    }

    void Update()
    {
        if (!isOnJourney && GameManager.instance.player.readyToStart)
            Init();
        else if (isOnJourney && !GameManager.instance.player.readyToStart)
            animator.SetTrigger("idle");
        if (GameManager.instance.player.levelEnded && !levelComplete)
            levelComplete = true;
    }

    void Init()
    {
        isOnJourney = true;
        animator.SetTrigger("balanceo");
        transform.DOLocalPath(v4JourneyPoints.ToArray(), levelData.journeyTime, PathType.CatmullRom, PathMode.Full3D).SetLookAt(0.01f).SetEase(Ease.InOutSine)
            .OnComplete(() =>
            {
                animator.SetTrigger("idle");
                GameManager.instance.caronteOnEndPos = true;
            });
    }
}
